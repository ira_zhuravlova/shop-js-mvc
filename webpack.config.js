var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');

module.exports = {
    entry: path.resolve('src/js', 'app.js'),
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/dist',
        filename: 'bundle.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel?presets=es2015', exclude: /node_modules/},
            {test: /\.html$/, loader: 'html'},
            {test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css')},
            {test: /\.scss/, loader: ExtractTextPlugin.extract('style', 'css!sass?sourceMap')},
            {test: /\.(jpe?g|png|gif|svg|woff|woff(2)?|eot|ttf)$/i, loader: 'file?name=[path][name].[ext]'},
            {test: /jquery-mousewheel/, loader: "imports?define=>false&this=>window"},
            {test: /malihu-custom-scrollbar-plugin/, loader: "imports?define=>false&this=>window"}
        ]
    },
    devServer: {
        port: 8081
    },
    plugins: [
        new ExtractTextPlugin('bundle.css')
    ]
};