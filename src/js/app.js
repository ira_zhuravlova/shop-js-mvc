
require('../scss/app.scss');
require('../scss/swiper.min.css');
require('../scss/jquery.mCustomScrollbar.css');


var catalogTemplate = require('./view/templates/catalog.html');
var productTemplate = require('./view/templates/product.html');
var basketTemplate = require('./view/templates/basket.html');

var StartView = require('./view/startView');
var CatalogView = require('./view/catalogView');
var ProductView = require('./view/itemView');
var BasketView = require('./view/shoppingBagView');

var GoodsModel = require('./model/goodsModel');

var StartController = require('./controller/startController');
var CatalogController = require('./controller/catalogController');
var ItemController = require('./controller/itemController');
var ShoppingBagController = require('./controller/shoppingBagController');


function App() {

  var startView = new StartView(catalogTemplate);
  var catalogView = new CatalogView(catalogTemplate);
  var productView = new ProductView(productTemplate);
  var basketView = new BasketView(basketTemplate);

  var goodsModel = new GoodsModel();

  var startController = new StartController(goodsModel, startView);
  var catalogController = new CatalogController(goodsModel, catalogView);
  var itemController = new ItemController(goodsModel, productView);
  var shoppingBagController = new ShoppingBagController(goodsModel, basketView);

  startController.init();
  catalogController.init();
  itemController.init();
  shoppingBagController.init();
}

App();