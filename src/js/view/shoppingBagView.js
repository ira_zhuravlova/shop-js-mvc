import $ from 'jquery';
var AbstractView = require('./abstractView');


function ShoppingBagView(template, popupTemplate) {

    this.ViewTemplate = template;
    this.PopupTemplate = popupTemplate;

    this.deleteProductFromView = function(){
        $(event.target).closest("li").fadeOut('slow', function() {
            $(this).remove();
        });
    };

    this.settingsForProductValue = function(){

        var product = $(event.target).closest("ul").parent(),
            currentValue = event.target.value;

        if (currentValue <= 0) {
            event.target.value = "0";
            product.addClass("shopping-bag__item--deleted");
        }
        else {
            product.removeClass("shopping-bag__item--deleted");
        }


    };


    this.clearBasketInView = function(){
        $('.shopping-bag__product-list').fadeOut('slow', function(){
            $('.shopping-bag__product-list').remove();
        });

    };

}

ShoppingBagView.prototype = Object.create(AbstractView.prototype);
ShoppingBagView.prototype.constructor = ShoppingBagView;

module.exports = ShoppingBagView;