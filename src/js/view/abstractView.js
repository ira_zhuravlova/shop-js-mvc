var $ = require('jquery'),
    _ = require('lodash');

function AbstractView() {

}

AbstractView.prototype.render = function (data, appendPlace) {
    var currentTemplate;
    currentTemplate = _.template(this.ViewTemplate)({data: data});
    $(appendPlace).append(currentTemplate);
};

AbstractView.prototype.modalConfirm = function (confirmText) {
    var some = confirm(confirmText);
    return some;
};

AbstractView.prototype.toggleMainMenu = function(){
    var target = event.target;
    if($(target).hasClass('nav__button')){
        $('.nav__menu-list').toggle('slow');
    }

};


AbstractView.prototype.renderTotalCost = function(cost, appendPlace){
    $(appendPlace).html(cost + " $");

};

AbstractView.prototype.renderTotalAmount = function(amount, appendPlace){
    $(appendPlace).html('('+amount+')');
};



module.exports = AbstractView;