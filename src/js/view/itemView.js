var $ = require('jquery');
var AbstractView = require('./abstractView');

ItemView.prototype = Object.create(AbstractView.prototype);
ItemView.prototype.constructor = ItemView;

function ItemView(itemTemplate) {
    this.ViewTemplate = itemTemplate;

    this.changePhoto = function () {
        var target = event.target,
            bigPhoto = $('.product__big-photo')[0],
            listSmallPhotos = $('.product__item'),
            liElement = $(target).parent();

        if ($(target).hasClass('product__small-photo')) {
            $(listSmallPhotos).removeClass('product__item--selected-photo');
            $(liElement).addClass('product__item--selected-photo');
            $(bigPhoto).fadeOut("slow", function(){
                bigPhoto.src = target.src.replace('small', 'big');
            });
            $(bigPhoto).fadeIn("slow");

        }
    };

}

module.exports = ItemView;
