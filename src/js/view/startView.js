var AbstractView = require('./abstractView');


function StartView(template) {

    this.ViewTemplate = template;

}

StartView.prototype = Object.create(AbstractView.prototype);
StartView.prototype.constructor = StartView;



module.exports = StartView;