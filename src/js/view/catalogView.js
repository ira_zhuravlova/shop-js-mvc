var AbstractView = require('./abstractView');
var $ = require('jquery');


function CatalogView(template) {
    
    this.ViewTemplate = template;
    
    this.toggleCatalogMenu = function(){
            $('.filters__filters-list').toggle('slow');
    };

    this.toggleCatalogMenuElements = function(){
        var target = event.target;
        $(target).closest('li').find('.filters__element-list').toggle('slow');
 
    };

    this.toggleActiveButton = function(){
        var target = $(event.target),
            filterType = $(target).parent()[0].id,
            filterCurrentType = $(event.target).html(),
            headingFiltersList = $('.filters__filters-heading-list').children(),
            filtersList = $(target).parent().children(),
            min = $(target).data('min'),
            max = $(target).data('max');


        $(filtersList).removeClass('filters__item--selected');
        $(target).addClass('filters__item--selected');
        
        for(var i = 0; i<headingFiltersList.length; i++){
            if(headingFiltersList[i].id === 'heading-'+filterType){
                $(headingFiltersList[i]).html(filterCurrentType);
                if(filterType === 'price'){
                    $(headingFiltersList[i]).data('min', min);
                    $(headingFiltersList[i]).data('max', max);
                    
                }
                if(filterCurrentType === 'Not selected,'){
                    $(headingFiltersList[i]).removeClass('filters__heading-element--selected');
                }
                else{
                    $(headingFiltersList[i]).addClass('filters__heading-element--selected');
                }
            }
        }


        var headingType = $(target).parent().closest('li').find('.filters__type'),
            headingTitle = $(target).parent().closest('li').find('.filters__title');

        if(filterCurrentType.indexOf(',') !== -1){
            filterCurrentType = filterCurrentType.slice(0, -1);
        }
        if(filterCurrentType === 'Not selected'){
            $(headingType).addClass('filters__type--not-selected');
        }
        else{
            $(headingType).removeClass('filters__type--not-selected');
        }

        $(headingTitle).addClass('filters__title--selected');
        $(headingType).html(filterCurrentType);

        if($(window).width() > 1024){
            $(target).parent().toggle('slow');
        }



    };

    this.renderFilterData = function (data, appendPlace) {
        var currentTemplate;
        if(data.length === 0){
            $(appendPlace).html('<h3 class="catalog__not-found-heading">Product not found. Please change filter request.</h3>');
        }else{
            currentTemplate = _.template(this.ViewTemplate)({data: data});
            $(appendPlace).html(currentTemplate);
        }
    };

}



CatalogView.prototype = Object.create(AbstractView.prototype);
CatalogView.prototype.constructor = CatalogView;



module.exports = CatalogView;
