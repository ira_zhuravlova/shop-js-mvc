var $ = require('jquery');

function GoodsModel() {

    this.setBasket = function () {
        var basket = localStorage.getItem('basket');
        if (!basket) {
            var basket = JSON.stringify([]);
            localStorage.setItem('basket', basket);
        }
    };

    this.getGoodsList = function (category) {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:8081/json/' + category + '.json',
            dataType: 'json'
        })
    };

    this.calculateTotalCost = function () {
        var newProductTotalCost = 0,
            currentTotalCost,
            basket = localStorage.getItem('basket');

        basket = JSON.parse(basket);

        basket.forEach(function (element) {
            currentTotalCost = element.value * element.price;
            newProductTotalCost += currentTotalCost;
        });

        return newProductTotalCost.toFixed(2);
    };

    this.calculateTotalAmount = function () {
        var newTotalAmount = 0,
            basket = localStorage.getItem('basket');

        basket = JSON.parse(basket);

        basket.forEach(function (element) {
            newTotalAmount += +element.value;
        });


        return newTotalAmount;
    };

    this.setLocalStorageCategory = function () {
        var target = event.target,
            category = $(target).html();

        if ($(target).hasClass('nav__link')) {
            localStorage.setItem('category', category);
        }

    };


    this.selectedFiltersObject = {};

    this.checkNotSelected = function () {
        var target = event.target,
            selectedType = $(target).parent()[0].id;

        if ($(target).html() === 'Not selected,') {
            for (var type in this.selectedFiltersObject) {
                if (type === selectedType) {
                    delete this.selectedFiltersObject[selectedType];
                }
            }
        }
    };

    this.sortGoods = function () {
        var selectedFiltersArray = $('.filters__heading-element--selected'),
            category = localStorage.getItem('category'),
            minSelectedPrice,
            maxSelectedPrice;

        for (var i = 0; i < selectedFiltersArray.length; i++) {
            var selectedType = selectedFiltersArray[i].id.slice(8),
                selectedValue = $(selectedFiltersArray[i]).html();
            if (selectedValue.indexOf(',') !== -1) {
                selectedValue = selectedValue.slice(0, -1);
            }

            this.selectedFiltersObject[selectedType] = selectedValue;

            if (selectedType === 'price') {
                minSelectedPrice = $(selectedFiltersArray[i]).data('min');
                maxSelectedPrice = $(selectedFiltersArray[i]).data('max');
                console.log(minSelectedPrice);
            }

        }


        var filterArray = localStorage.getItem('filterArray');
        filterArray = JSON.parse(filterArray);
        var filtersObj = this.selectedFiltersObject;


        for (var type in filtersObj) {

            filterArray = filterArray.filter(function (item, index) {

                var itemValueArr = item[type];
                if (typeof(itemValueArr) === 'object') {
                    return itemValueArr.some(function (element, i) {
                        return filtersObj[type] === element;
                    });

                }
                else {

                    if (type === 'price') {
                        var checkMin = (itemValueArr > minSelectedPrice || itemValueArr == minSelectedPrice) || (minSelectedPrice === undefined),
                            checkMax = maxSelectedPrice > itemValueArr || itemValueArr == maxSelectedPrice;
                        return checkMax && checkMin;
                    }
                    else {
                        return filtersObj[type] === itemValueArr;
                    }
                }
            });
        }
        return filterArray;
    };


    this.setLocalStorageProductId = function () {
        var target = event.target,
            productId = $(target).closest('li')[0].id;

        if ($(target).hasClass('goods-list__product-img') || $(target).hasClass('goods-list__product-name')) {
            localStorage.setItem('productId', productId);
        }
    };

    this.resetProductSettings = function () {
        localStorage.removeItem('productSize');
        localStorage.removeItem('productColor');
    };

    this.getSize = function () {
        var sizesList = $('.product__size'),
            currentSize = event.target;

        if ($(currentSize).hasClass('product__size')) {
            $(sizesList).removeClass("product__size--selected-element");
            $(currentSize).addClass("product__size--selected-element");
            localStorage.setItem('productSize', $(currentSize).html());
        }
    };

    this.getColor = function () {
        if (event.target.classList.contains('product__color')) {
            var colorsList = document.getElementsByClassName("product__color");
            var currentColor = event.target;
            $(colorsList).removeClass("product__color--selected-element");
            $(currentColor).addClass("product__color--selected-element");
            localStorage.setItem('productColor', currentColor.innerHTML);
        }
    };

    this.isBasketEmpty = function (basket) {
        if (basket.length === 0) {
            return true;
        }
        return false;
    };

    this.isParamsDifferent = function (basket, productId, productSize, productColor) {
        try {
            for (var i = 0; i <= basket.length; i++) {
                if (basket[i].id !== productId || basket[i].size !== productSize || basket[i].color !== productColor) {
                    return true;
                }

            }
            return false;
        }
        catch (err) {
            return false;
        }

    };


    this.setProductDataInBasket = function () {

        var category = localStorage.getItem('category'),
            productId = localStorage.getItem('productId'),
            productSize = localStorage.getItem('productSize'),
            productColor = localStorage.getItem('productColor'),
            productData,
            basket = localStorage.getItem('basket');

        basket = JSON.parse(basket);

        var isBasketEmpty = this.isBasketEmpty(basket),
            isParamsDifferent = this.isParamsDifferent(basket, productId, productSize, productColor);

        if (isBasketEmpty || isParamsDifferent) {

            this.getGoodsList(category)
                .success((res) => {

                    for (var i = 0; i < res.length; i++) {
                        if (res[i].id === productId) {
                            productData = res[i];
                        }
                    }

                    productData.size = productSize;
                    productData.color = productColor;

                    basket.push(productData);

                    var basketJSON = JSON.stringify(basket);
                    localStorage.setItem('basket', basketJSON);
                })
                .error((err) => {
                    console.log(err);
                });
        }
        else {

            for (var i = 0; i < basket.length; i++) {

                if (basket[i].id === productId && basket[i].size === productSize && basket[i].color === productColor) {
                    basket[i].value = parseInt(basket[i].value) + 1;
                    var basketJSON = JSON.stringify(basket);
                    localStorage.setItem('basket', basketJSON);
                }
            }
        }
    };

    this.setProductValueInBasket = function () {

        var productName = $(event.target).closest("ul").find('.shopping-bag__item-heading').html(),
            productSize = $(event.target).closest("ul").find('.shopping-bag__item-size').html(),
            productColor = $(event.target).closest("ul").find('.shopping-bag__item-color').html(),
            currentValue = event.target.value,
            basket = localStorage.getItem('basket');

        basket = JSON.parse(basket);

        for (var i = 0; i < basket.length; i++) {
            if (basket[i].name === productName && basket[i].size === productSize && basket[i].color === productColor) {
                basket[i].value = currentValue;
            }
        }

        var basketJSON = JSON.stringify(basket);
        localStorage.setItem('basket', basketJSON);

    };

    this.deleteProductFromBasket = function () {

        var target = event.target,
            productName = $(target).prev().find('.shopping-bag__item-heading').html(),
            productSize = $(target).prev().find('.shopping-bag__item-size').html(),
            productColor = $(target).prev().find('.shopping-bag__item-color').html(),
            basket = localStorage.getItem('basket');
        basket = JSON.parse(basket);
        for (var i = 0; i < basket.length; i++) {
            if (basket[i].name === productName && basket[i].size === productSize && basket[i].color === productColor) {
                basket.splice(i, 1);
            }
        }
        var basketJSON = JSON.stringify(basket);
        localStorage.setItem('basket', basketJSON);

    };

    this.clearBasketInLocalStorage = function () {
        var basket = localStorage.getItem('basket');
        basket = JSON.parse(basket);
        basket.length = 0;
        var basketJSON = JSON.stringify(basket);
        localStorage.setItem('basket', basketJSON);
    };


}

module.exports = GoodsModel;


