var $ = require('jquery');

function AbstractController(){

}

AbstractController.prototype.page = $('.page');

AbstractController.prototype.renderPage = function(){
    console.log("URL");
};

AbstractController.prototype.setTotalCost = function(appendTotalCostPlace){
    var newCost = this.model.calculateTotalCost();
    this.view.renderTotalCost(newCost, appendTotalCostPlace);
};

AbstractController.prototype.setTotalAmount = function(appendTotalAmountPlace){
    var newAmount = this.model.calculateTotalAmount();
    this.view.renderTotalAmount(newAmount, appendTotalAmountPlace);
};


module.exports = AbstractController;