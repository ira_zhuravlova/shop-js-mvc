
var $ = require('jquery'),
    AbstractController = require('./abstractController'),
    Popup = require('../components/popup/popupMain.js');

function ItemController(model, view) {

    this.model = model;
    this.view = view;
    this.popup = new Popup('.wrapper', 3000);

    this.init = function () {
        this.model.setBasket();
        this.renderPage();
    };


    this.renderPage = function () {
        var url = window.location.pathname,
            category = localStorage.getItem('category'),
            productId = localStorage.getItem('productId'),
            productData;

        if (url === '/item.html') {

            this.model.getGoodsList(category)
                .success((res) => {
                    for (var i = 0; i < res.length; i++) {
                        if (res[i].id === productId) {
                            productData = res[i];
                        }
                    }

                    this.view.render(productData, '.product');


                })
                .error((err) => {
                    console.log(err);
                });

            this.setPageSettings();

        }


    };

    this.setPageSettings = function () {
        this.model.resetProductSettings();
        this.setTotalCost('.header__products-price');
        this.setTotalAmount('.header__products-quantity');
        this.setEvents();
    };

    this.setEvents = function () {
        $('.header__nav').on("click", this.model.setLocalStorageCategory);
        $('.nav__nav-buttons').on("click", this.view.toggleMainMenu);
        $('.product').on("click", this.view.changePhoto);
        $('.product').on("click", this.model.getSize);
        $('.product').on("click", this.model.getColor);
        $('.product').on("click", this.setProductData.bind(this));
    };

    this.setProductData = function () {
        var target = event.target,
            productSize = localStorage.getItem('productSize'),
            productColor = localStorage.getItem('productColor');

        if ($(target).hasClass('product__add-button')) {
            if (productSize) {
                if (productColor) {
                    this.model.setProductDataInBasket();
                }

                else {
                    event.preventDefault();
                    this.popup.activatePopup({
                        title: 'Product is not added to the cart',
                        message: 'Please, select color'
                    });
                }

            }
            else {
                event.preventDefault();
                this.popup.activatePopup({
                    title: 'Product is not added to the cart',
                    message: 'Please, select size'
                });
                
            }
        }

    };
}

ItemController.prototype = Object.create(AbstractController.prototype);
ItemController.prototype.constructor = ItemController;

module.exports = ItemController;

