var $ = require('jquery'),
    AbstractController = require('./abstractController'),
    Scrollbar = require('../utils/scrollbarUtil');

function CatalogController(model, view) {

    this.model = model;
    this.view = view;

    this.init = function() {
        this.model.setBasket();
        this.renderPage();

    };

    this.renderPage = function() {
        var url = window.location.pathname,
            category = localStorage.getItem('category'),
            bottomData,
            resJSON;
        
        if (url === '/catalog.html') {

            this.model.getGoodsList(category)
                .success((res) => {

                    resJSON = JSON.stringify(res);
                    localStorage.setItem('filterArray', resJSON);

                    bottomData = res.splice(4, res.length);

                    this.view.render(res, '.catalog__top-catalog');
                    this.view.render(bottomData, '.catalog__bottom-catalog');

                })
                .error((err) => {
                    console.log(err);
                });

            this.setPageSettings();
        }

    };

    this.setPageSettings = function(){
        this.setTotalCost('.header__products-price');
        this.setTotalAmount('.header__products-quantity');
        this.setActiveCategory();
        this.setEvents();
        Scrollbar();
    };

    this.setEvents = function() {
        $('.header__nav').on("click", this.model.setLocalStorageCategory);
        $('.nav__nav-buttons').on("click", this.view.toggleMainMenu);
        $('.filters__heading-container').on("click", this.view.toggleCatalogMenu);
        $('.filters__element-heading').on("click", this.view.toggleCatalogMenuElements);
        $('.catalog').on("click", this.model.setLocalStorageProductId);
        $('.filters').on("click", this.filterGoods.bind(this));
    };

    this.setActiveCategory = function(){
      var activeCategory = localStorage.getItem('category'),
          navCategories = $('.nav__link'),
          category;

        for(var i=0; i< navCategories.length; i++){
            category = $(navCategories[i]).html();
            if(category === activeCategory){
                $(navCategories[i]).addClass('nav__link--active');
            }
        }

    };
    
    this.filterGoods = function(){
        var target = event.target;
        
        if($(target).hasClass('filters__item')){
            this.model.checkNotSelected();
            this.view.toggleActiveButton();
            var filterData = this.model.sortGoods();
            this.view.renderFilterData(filterData, '.catalog');

        }
    }
}

CatalogController.prototype = Object.create(AbstractController.prototype);
CatalogController.prototype.constructor = CatalogController;

module.exports = CatalogController;

