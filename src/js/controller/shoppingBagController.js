var $ = require('jquery'),
    AbstractController = require('./abstractController'),
    Popup = require('../components/popup/popupMain.js');

function ShoppingBagController(model, view) {

    this.model = model;
    this.view = view;
    this.popup = new Popup('.wrapper', 3000);

    this.init = function() {
        this.model.setBasket();
        this.renderPage();

    };


    this.renderPage = function() {
        var url = window.location.pathname,
            basket = localStorage.getItem('basket');

        if (url === '/shopping-bag.html') {
            basket = JSON.parse(basket);
            this.view.render(basket, '.shopping-bag');
            this.setPageSettings();
        }

    };

    this.setPageSettings = function(){
        this.setTotalCost('.header__products-price');
        this.setTotalAmount('.header__products-quantity');
        this.setEvents();
    };

    this.setEvents = function() {
        $('.header__nav').on("click", this.model.setLocalStorageCategory);
        $('.nav__nav-buttons').on("click", this.view.toggleMainMenu);
        $('.shopping-bag').on("change", this.setProductValue.bind(this));
        $('.shopping-bag').on("click", this.deleteProduct.bind(this));
        $('.shopping-bag').on("click", this.buyGoods.bind(this));
    };

    this.setProductValue = function(){
        var target = event.target,
            newCost,
            appendTotalCostPlace = $('.page .shopping-bag__total-cost-value')[0];

        if ($(target).hasClass('shopping-bag__item-quantity')) {
            this.view.settingsForProductValue();
            this.model.setProductValueInBasket();
            newCost = this.model.calculateTotalCost();
            this.view.renderTotalCost(newCost, appendTotalCostPlace);
            this.view.renderTotalCost(newCost, '.header__products-price');
            this.setTotalAmount('.header__products-quantity');
        }
    };

    this.deleteProduct = function() {
        var target = event.target,
            confirmDelete,
            newCost,
            appendTotalCostPlace = $('.page .shopping-bag__total-cost-value')[0];

        if ($(target).hasClass('shopping-bag__remove-item')) {
            confirmDelete = this.view.modalConfirm("Are you sure you want to remove this item?");
            if (confirmDelete) {
                this.model.deleteProductFromBasket();
                this.view.deleteProductFromView();
                newCost = this.model.calculateTotalCost();
                this.view.renderTotalCost(newCost, appendTotalCostPlace);
                this.view.renderTotalCost(newCost, '.header__products-price');
                this.setTotalAmount('.header__products-quantity');
            }
        }
    };

    this.buyGoods = function(){
        var target = event.target,
            appendTotalCostPlace = $('.page .shopping-bag__total-cost-value')[0],
            basket = localStorage.getItem('basket');
            basket = JSON.parse(basket);


        if ($(target).hasClass('shopping-bag__buy-button')) {
            event.preventDefault();
            if(basket.length === 0){
                this.popup.activatePopup({
                    title: 'Bag is empty',
                    message: 'Please, select products'
                });
            }
            else{
                this.model.clearBasketInLocalStorage();
                this.view.clearBasketInView();
                this.popup.activatePopup({
                    title: 'Order has been accepted',
                    message: 'Thank you, stay with us'
                });
                this.setTotalCost(appendTotalCostPlace);
                this.setTotalCost('.header__products-price');
                this.setTotalAmount('.header__products-quantity');
            }

        }
    }
}

ShoppingBagController.prototype = Object.create(AbstractController.prototype);
ShoppingBagController.prototype.constructor = ShoppingBagController;

module.exports = ShoppingBagController;

