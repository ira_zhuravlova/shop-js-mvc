var $ = require('jquery'),
    AbstractController = require('./abstractController'),
    Swiper = require('../utils/swiperUtil');

function StartController(model, view) {

    this.model = model;
    this.view = view;

    this.init = function() {
        this.model.setBasket();
        this.renderPage();
    };

    this.renderPage = function() {
        var url = window.location.pathname,
            category = 'goodsStartPage';

        if (url === '/start.html') {
            this.model.getGoodsList(category)
                .success((res) => {
                    this.view.render(res, '.new-arrivals__goods-list');
                })
                .error((err) => {
                    console.log(err);
                });

            this.setPageSettings();
        }

    };

    this.setPageSettings = function(){
        this.setTotalCost('.header__products-price');
        this.setTotalAmount('.header__products-quantity');
        this.setEvents();
        Swiper();
    };

    this.setEvents = function() {
        $('.header__nav').on("click", this.model.setLocalStorageCategory);
        $('.nav__nav-buttons').on("click", this.view.toggleMainMenu);
        $('.new-arrivals__goods-list').on("click", this.setLocalStorageCategoryAndId.bind(this));
    };

    this.setLocalStorageCategoryAndId = function(){
        var target = event.target,
            url = window.location.pathname;

        if (url === '/start.html' && $(target).hasClass('goods-list__product-img') || $(target).hasClass('goods-list__product-name')) {
            this.model.setLocalStorageProductId();
            localStorage.setItem('category', 'woman');
        }

    }

}

StartController.prototype = Object.create(AbstractController.prototype);
StartController.prototype.constructor = StartController;

module.exports = StartController;

