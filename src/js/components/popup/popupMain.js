var popupTemplate = require('./popup.html'),
    _ = require('lodash'),
    $ = require('jquery');

function Popup(appendPlace, delayTime) {

    this.appendPlace = appendPlace;
    this.delayTime = delayTime;

    var currentTemplate;
    
    this.activatePopup = function(popupData){
        currentTemplate = _.template(popupTemplate)({data: popupData});

        $(this.appendPlace).append(currentTemplate);

        $('.popup__close-button--close-state').on('click', function(){
            $('.popup').fadeOut('slow', function () {
                $(this).remove();
            })
        });

        setTimeout(function () {
            $('.popup').fadeOut('slow', function () {
                $(this).remove();
            })
        }, this.delayTime);
    };
    
}

module.exports = Popup;