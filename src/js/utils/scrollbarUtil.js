var $ = require('jquery');

require("jquery-mousewheel")($);
require('malihu-custom-scrollbar-plugin')($);

var activateScrollbar = function() {
    $(".content").mCustomScrollbar({
        axis: "x",
        advanced: {
            autoExpandHorizontalScroll: true
        },
        autoHideScrollbar: true,
        documentTouchScroll: true
    });
};

module.exports = activateScrollbar;