var Swiper = require('swiper');

var activateSwiper = function(){
    var mySwiper = new Swiper ('.swiper-container', {
        loop: true,
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
    
};


module.exports = activateSwiper;
